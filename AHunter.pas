unit AHunter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GLScene, GLVectorFileObjects, GLObjects, GLCoordinates, GLMaterial,
  GLCadencer, GLCrossPlatform, BaseClasses, GLWin32Viewer, StdCtrls,
  GLGeomObjects, GLNavigator, GLSpaceText, GLSkyBox,TGA, GLCollision,GLfileoBJ,
  ExtCtrls, GLTerrainRenderer, GLHeightData, GLBumpmapHDS;

type
  TForm1 = class(TForm)
    GLSceneViewer1: TGLSceneViewer;
    GLScene1: TGLScene;
    GLCadencer1: TGLCadencer;
    GLMaterialLibrary1: TGLMaterialLibrary;
    GLCamera1: TGLCamera;
    GLLightSource1: TGLLightSource;
    GLDummyCube1: TGLDummyCube;
    WeaponMod: TGLActor;
    FirsP: TGLCamera;
    GLNavigator1: TGLNavigator;
    GLUserInterface1: TGLUserInterface;
    GLLightSource2: TGLLightSource;
    GLDummyCube2: TGLDummyCube;
    GLSkyBox1: TGLSkyBox;
    Bullets: TGLDummyCube;
    CollisionManager1: TCollisionManager;
    FPSTimer: TTimer;
    GLBitmapHDS1: TGLBitmapHDS;
    GLBumpmapHDS1: TGLBumpmapHDS;
    GLTerrainRenderer1: TGLTerrainRenderer;
    SpaceInvader: TGLDummyCube;
    GLFreeForm1: TGLFreeForm;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    //procedure GLSceneViewer1MouseDown(Sender: TObject; Button: TMouseButton;
      //Shift: TShiftState; X, Y: Integer);
    //procedure GLSceneViewer1MouseMove(Sender: TObject; Shift: TShiftState; X,
      //Y: Integer);
    procedure GLCadencer1Progress(Sender: TObject; const deltaTime,
      newTime: Double);
   procedure HandleKeys(const deltaTime: Double);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FPSTimerTimer(Sender: TObject);
        procedure AddInvaders;
    procedure CollisionManager1Collision(Sender: TObject; object1,
      object2: TGLBaseSceneObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  mdy: Integer;
  mdx: Integer;
  interpolated_height: Single;
  height_space: Single;
  time:single;
  List:TStrings;
  BCount:Integer;

implementation

{$R *.dfm}

uses VectorGeometry, Jpeg, GLKeyboard, GLFileMD2;

const
  cWalkStep = 15;     // velocidad de caminado
  cStrafeStep = 10;   // velocidad de caminado de lado
  cRunBoost = 2;      // speed boost when running
  cNbInvaders = 20;   //Cantidad de MOBS de tipo space invaders
  cSpread = 10000;       //Determina el "radio" de esparcimiento de los MOBS


procedure TForm1.Button1Click(Sender: TObject);
begin
  GLSceneViewer1.Camera:=FirsP;
end;

procedure TForm1.CollisionManager1Collision(Sender: TObject; object1,
  object2: TGLBaseSceneObject);
begin
 ShowMessage('Colision entre '+object1.Name+' y '+object2.Name);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
GLCadencer1.Enabled:=False;
  List.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  //Carga del terreno
  GLBitmapHDS1.MaxPoolSize:=8*1024*1024;
  GLBitmapHDS1.Picture.LoadFromFile('terrain\terrain.bmp');
  GLTerrainRenderer1.TilesPerTexture:=256/GLTerrainRenderer1.TileSize;
  GLTerrainRenderer1.Material.Texture.Image.LoadFromFile ('terrain\texture_terrain.jpg');
  GLTerrainRenderer1.Material.Texture.Disabled := False;

//Almacena el numero de bala e incrementa con cada bala disparada
  BCount:=0;
  List:=TStringList.Create;
  time:=0.0;
// Carga el modelo en GLScene del jugador
   WeaponMod.LoadFromFile('models\weapon.md2');
   WeaponMod.Material.Texture.Image.LoadFromFile('models\weapon.jpg');
   WeaponMod.Scale.SetVector(0.04, 0.04, 0.04, 0);
   //Carga el archivo del MOB space invaders
   GLFreeForm1.LoadFromFile('models\Space_Invader\Space_Invader.obj');
   //Escala del space invader
   SpaceInvader.Scale.SetVector(0.1, 0.1, 0.1, 0);
   // Duplica los MOBS invaders (pero no los datos de la freeform1 !)
     //AddInvaders;
//Carga el modelo en GLscene de las balas (por cuestiones de memoria no se implementan free forms para las balas)
   //bullet.LoadFromFile('models\Bullet\SA_45ACP_Example.obj');
   //Bullet.Material.Texture.Image.LoadFromFile('models\Bullet\SA_45ACP_Diffuse.tga');
   //bullets.Scale.SetVector(0.2, 0.2, 0.2, 0);
    //Carga el modelo en GLscene de las balas
   {Textura de las balas}
   //bullets.Material.Texture.Image.LoadFromFile('models\Bullet\SA_45ACP_Diffuse.tga');

   // Define la propiedad de la animacion inicial (Default)
   WeaponMod.AnimationMode:=aamLoop;
   WeaponMod.StartFrame:=1;
   WeaponMod.EndFrame:=20;
   WeaponMod.FrameInterpolation:=afpLinear;
   GLUserInterface1.MouseLookActive:=True;
end;

procedure TForm1.FPSTimerTimer(Sender: TObject);
begin
      //Se utiliza para que cada que haya una actualizacion el texto no se repita (se actualiza en el mismo lugar)
      //Genera el texto que se muestra al tope de la ventana
          Caption := GLSceneViewer1.FramesPerSecondText;
      //Reinicia los contadores de FPS text
          GLSceneViewer1.ResetPerformanceMonitor;
end;

procedure TForm1.HandleKeys(const deltaTime: Double);


begin

   // This function uses asynchronous keyboard check (see Keyboard.pas)
   if IsKeyDown(VK_ESCAPE) then
   application.Terminate;
   //Posicionar camara

   //Tercera persona
   if IsKeyDown(VK_F7) then
   begin
      GLSceneViewer1.Camera:=GLCamera1;
   end;
   //Primera persona
   if IsKeyDown(VK_F8) then
   begin
      GLSceneViewer1.Camera:=FirsP;
   end;

   with GLDummyCube1.Position do begin
        interpolated_height:=GLTerrainRenderer1.InterpolatedHeight(AsVector);
   if Z<>interpolated_height then
       Z:=interpolated_height+4;
   end;


end;


procedure TForm1.GLCadencer1Progress(Sender: TObject; const deltaTime,
  newTime: Double);
var
  startf : integer;        //Frames de inicio y de fin para asignacion
  endf : integer;
  boost : Single;
  i:integer;
  PActor: TVector;
  A, B: TVector;
  Bullet:TGLSphere;
begin

time:=time+deltatime;
    CollisionManager1.CheckCollisions;

      For I:=0 To List.Count-1 Do
        bullets.FindChild(list[i],true).Free;
        list.clear;

      for i:=0 to bullets.Count-1 do
        begin
          //Controla la velocidad de movimiento de las balas
          bullets.Children[i].Move(deltatime*500);
        end;

    //Aqui se controla la cadencia de disparo
    if ((IsKeyDown(VK_LBUTTON))and(time>0.3)) Then
   begin
      WeaponMod.StartFrame:=21;
      WeaponMod.EndFrame:=25;
      time:=0.0;
      Bullet:=TGLSphere(Bullets.AddNewChild(TGLSphere));
      bullet.Scale.SetVector(0.2, 0.2, 0.2, 0);


        With GetOrCreateCollision(Bullet) Do
        begin
            BoundingMode:=cbmSphere;
            Manager:=CollisionManager1;
        end;

   Bullet.Name:='Bullet'+IntToStr(BCount); //Nombre del hijo del objeto bala
   // Bullet.Name:='Bullet';
   A := firsp.AbsoluteDirection;
   ScaleVector(A, 100);
   B := VectorSubtract(firsp.AbsolutePosition, weaponmod.AbsolutePosition);
   Bullet.AbsolutePosition := weaponmod.AbsolutePosition;
   Bullet.AbsoluteDirection := VectorAdd(A, B);
   Bullet.Move(4);
   Inc(BCount);

   end;

    //Si la distancia entre el MOB y el jugador es menos a 60, que el MOB cambie la direccion hacia el jugador
    {if (GLDummyCube1.DistanceTo(SpaceInvader)<120.0) then
      begin
        SpaceInvader.Direction:=GLDummyCube1.Direction;
      end;}


   HandleKeys(deltaTime);
   GLUserInterface1.Mouselook;
   GLSceneViewer1.Invalidate;
   GLUserInterface1.MouseUpdate;

   {MOVIMIENTO DEL JUGADOR}

   // if nothing specified, we are standing (defined by animation frames)
    startf:=1;
    endf:=20;

    //Si estamos corriendo asignamos a las animaciones un intervalo mas rapido
   if IsKeyDown(VK_SHIFT) then
   begin
      WeaponMod.Interval:=75;
      boost:=cRunBoost*deltaTime

   end

   else

   begin
      WeaponMod.Interval:=125;
      boost:=deltaTime;
   end;

   // are we advaning/backpedaling ?
   if IsKeyDown(VK_UP) or IsKeyDown(87) then
   begin
      GLNavigator1.MoveForward(cWalkStep*boost);
      startf:=26;
      endf:=33;
   end;
   if IsKeyDown(VK_DOWN) or IsKeyDown(83) then
   begin
      GLNavigator1.MoveForward(-cWalkStep*boost);
      startf:=26;
      endf:=33;
   end;

   if IsKeyDown(VK_LEFT) or IsKeyDown(65) then
   begin
      GLNavigator1.StrafeHorizontal(-cStrafeStep * boost);
      startf:=26;
      endf:=33;
   end;
   if (IsKeyDown(VK_RIGHT) or IsKeyDown(68)) then
   begin
      GLNavigator1.StrafeHorizontal(cStrafeStep * boost);
      startf:=26;
      endf:=33;
   end;

      //Actualizar animaciones
      if WeaponMod.StartFrame<>startf then begin
      WeaponMod.StartFrame := startf;
      WeaponMod.EndFrame := endf;
   end;
   {MOVIMIENTO DEL MOB}

   if (SpaceInvader.DistanceTo (GLDummyCube1) > 0) then
   begin
      with SpaceInvader.Position do begin
         height_space:=GLTerrainRenderer1.InterpolatedHeight(AsVector);
         if Z<>height_space then
          Z:=height_space+6;
      end;
      PActor    := GLDummyCube1.Position.AsVector;
      PActor[2] := 0;
      SpaceInvader.PointTo (PActor, ZHmgVector);
      SpaceInvader.Move (20 * deltaTime);
   end;

end;

procedure TForm1.AddInvaders;
var
   i : Integer;
   proxy : TGLProxyObject;
   s : TVector;
   f : Single;
begin
   // spawn some more mushrooms using proxy objects
   for i:=0 to cNbInvaders-1 do begin
      // create a new proxy and set its MasterObject property
      proxy:=TGLProxyObject(SpaceInvader.AddNewChild(TGLProxyObject));
      with proxy do begin
         ProxyOptions:=[pooObjects]; //Para jalar todos los objetos de GLScene
         MasterObject:=GLFreeForm1;   //Define al objeto maestro (de donde se sacan los clones)como la freeform 1
         // retrieve reference attitude
         Direction:=GLFreeForm1.Direction;
         Up:=GLFreeForm1.Up;
         // randomize scale
        { s:=FreeForm1.Scale.AsVector;
         f:=(1*Random+1);
         ScaleVector(s, f);
         Scale.AsVector:=s;}
         // randomize position
         Position.SetPoint(Random(cSpread)-(cSpread/2),
                           GLFreeForm1.Position.z+0.8*f,
                           Random(cSpread)-(cSpread/2));
         // randomize orientation
         {RollAngle:=Random(360);
         TransformationChanged;}
      end;
   end;
end;



end.
