unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GLFPSMovement, GLNavigator, GLMaterial, ExtCtrls, GLCadencer,
  GLCrossPlatform, BaseClasses, GLScene, GLWin32Viewer;

type
  TForm1 = class(TForm)
    GLSceneViewer1: TGLSceneViewer;
    GLScene1: TGLScene;
    GLCadencer1: TGLCadencer;
    Timer1: TTimer;
    GLMaterialLibrary1: TGLMaterialLibrary;
    GLNavigator1: TGLNavigator;
    GLFPSMovementManager1: TGLFPSMovementManager;
    procedure GLCadencer1Progress(Sender: TObject; const deltaTime,
  newTime: Double);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.GLCadencer1Progress(Sender: TObject; const deltaTime,
  newTime: Double);
var
  movementScale: single;
begin
  movementScale := Movmanager.movementScale;

  //then update position according to keys being pressed
  if IsKeyDown('W') or IsKeyDown('Z') then
    behav.MoveForward(MovementScale * deltaTime);
  if IsKeyDown('S') then
    behav.MoveForward(-MovementScale * deltaTime);
  if IsKeyDown('A') or IsKeyDown('Q') then
    behav.StrafeHorizontal(-MovementScale * deltaTime);
  if IsKeyDown('D') then
    behav.StrafeHorizontal(MovementScale * deltaTime);

  //move up/down (for debugging)
  if IsKeyDown(VK_PRIOR) or IsKeyDown(VK_SPACE) then
    behav.StrafeVertical(MovementScale * deltaTime);
  if IsKeyDown(VK_NEXT) then
    behav.StrafeVertical(-MovementScale * deltaTime);

  //move bot
  if IsKeyDown('I') then
    behav2.MoveForward(MovementScale * deltaTime);
  if IsKeyDown('K') then
    behav2.MoveForward(-MovementScale * deltaTime);
  if IsKeyDown('J') then
    behav2.StrafeHorizontal(-MovementScale * deltaTime);
  if IsKeyDown('L') then
    behav2.StrafeHorizontal(MovementScale * deltaTime);
  if IsKeyDown('O') then
    behav2.StrafeVertical(MovementScale * deltaTime);
  if IsKeyDown('P') then
    behav.StrafeVertical(-MovementScale * deltaTime);

  if IsKeyDown(VK_LEFT) then
    behav.TurnHorizontal(-70 * deltatime);
  if IsKeyDown(VK_RIGHT) then
    behav.TurnHorizontal(70 * deltatime);
  if IsKeyDown(VK_UP) then
    behav.turnVertical(-70 * deltatime);
  if IsKeyDown(VK_DOWN) then
    behav.turnVertical(70 * deltatime);

  //update mouse view
  xangle := mouse.CursorPos.X - screen.Width / 2;
  yangle := mouse.CursorPos.Y - screen.Height / 2;
  setcursorpos(screen.width div 2, screen.Height div 2);
  behav.TurnHorizontal(xangle * 40 * deltaTime);
  behav.TurnVertical(-yangle * 20 * deltaTime);

  GLSceneViewer1.Invalidate;
end;

end.
