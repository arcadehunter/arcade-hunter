unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GLScene, GLWaterPlane, GLWin32Viewer, JPEG, GLCadencer,
  GLObjects, GLNavigator, GLSkydome, GLTexture,
  GLCelShader, StdCtrls, GLParticleFX, GLLinePFX, GLGraph, AsyncTimer,
  GLVectorFileObjects, GLFile3DS,
  ExtCtrls, GLHUDObjects, GLFPSMovement, VectorGeometry,
  GLGeomObjects, GLCollision,GLFilemd2,
  ComCtrls, GLMaterial, GLCoordinates, GLCrossPlatform, BaseClasses,
  GLKeyboard;
  //GLMisc,GLWin32FullScreenViewer, ;

type
  TForm1 = class(TForm)
    GLScene1: TGLScene;
    GLCadencer1: TGLCadencer;
    GLCylinder1: TGLCylinder;
    GLCylinder2: TGLCylinder;
    GLCylinder3: TGLCylinder;
    GLCylinder4: TGLCylinder;
    GLLightSource2: TGLLightSource;
    Camera:   TGLCamera;
    GLSphere1: TGLSphere;
    GLNavigator1: TGLNavigator;
    weap:     TGLFreeForm;
    GLHUDSprite1: TGLHUDSprite;
    GLUserInterface1: TGLUserInterface;
    GLDummyCube3: TGLDummyCube;
    GLSceneViewer1: TGLSceneViewer;
    FPSTimer: TTimer;
    World:    TGLDummyCube;
    GLMaterialLibrary1: TGLMaterialLibrary;
    CollisionManager1: TCollisionManager;
    bullets: TGLDummyCube;
    GLDummyCube1: TGLDummyCube;
    GLActor1: TGLActor;
    GLActor2: TGLActor;
    GLCube1: TGLCube;
    GLDummyCube2: TGLDummyCube;
    GLLightSource1: TGLLightSource;
    GLCube2: TGLCube;
    procedure FormCreate(Sender: TObject);
    procedure GLCadencer1Progress(Sender: TObject;
      const deltaTime, newTime: double);
    procedure FPSTimerTimer(Sender: TObject);
    procedure CollisionManager1Collision(Sender: TObject; object1,
      object2: TGLBaseSceneObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    f: Boolean;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  time:single;
  List:TStrings;
  BCount:Integer;
  
  
implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  BCount:=0;  //Almacena el numero de bala e incrementa con cada bala disparada
  List:=TStringList.Create;
  time:=0.0;
  With GLMaterialLibrary1.Materials.Items[0].Material.Texture Do
    begin
      TextureMode:=tmReplace;
      ImageAlpha:=tiaTopLeftPointColorTransparent;
    end;
  f := False;
  weap.LoadFromFile('512x512.3DS');
  weap.Material.Texture.Image.LoadFromFile('512x512.jpg');
  weap.Material.Texture.Disabled:=false;
  GLUserInterface1.MouseLookActivate;
  GLCube1.Material.Texture.Enabled := True;

  //////////////////////////////Actor

  GLActor1.LoadFromFile('tris.md2'); //Tris es el MOB
  GLActor1.Material.Texture.Image.LoadFromFile('warvet.bmp');
  GLActor1.Material.Texture.Disabled:=false;
  GLActor2.LoadFromFile('w_super90.md2'); //Es el brazo del MOB
  GLActor2.Material.Texture.Image.LoadFromFile('w_super90.bmp');
  GLActor2.Material.Texture.Disabled:=false;
  GLActor1.AnimationMode:=aamLoop;
  GLActor1.SwitchToAnimation('stand');
  GLActor2.Animations.Assign(GLActor1.Animations);
  GLActor2.Synchronize(GLActor1);





end;


procedure TForm1.GLCadencer1Progress(Sender: TObject; const deltaTime, newTime: double);
var
  CurrentSpeed:    Single;
  A, B: TVector;
  i:integer;
  Bullet:TGLSphere;

begin
  time:=time+deltatime;
  CollisionManager1.CheckCollisions;
  For I:=0 To List.Count-1 Do
    bullets.FindChild(list[i],true).Free;
  list.clear;
  for i:=0 to bullets.Count-1 do
    begin
      bullets.Children[i].Move(deltatime*100); //Controla la velocidad de movimiento de las balas
      if bullets.Children[i].DistanceTo(GLDummyCube3)>Camera.DepthOfView then
        list.Add(bullets.Children[i].Name);
    end;
  CurrentSpeed := 40;
  if IsKeyDown(VK_SHIFT) then
    CurrentSpeed := CurrentSpeed * 2; //Velocidad de movimiento cuando el jugador presiona shift
  if IsKeyDown('w') then
    GLNavigator1.MoveForward(CurrentSpeed * deltaTime);
  if IsKeyDown('s') then
    GLNavigator1.MoveForward(-CurrentSpeed * deltaTime);
  if IsKeyDown('a') then
    GLNavigator1.StrafeHorizontal(-CurrentSpeed * deltaTime);
  if IsKeyDown('d') then
    GLNavigator1.StrafeHorizontal(CurrentSpeed * deltaTime);
  if IsKeyDown(VK_ESCAPE) then
    Close;
  GLUserInterface1.MouseLook;
  GLUserInterface1.MouseUpdate;
  //Esta validacion controla la creacion de las balas
  if ((IsKeyDown(VK_LBUTTON))and(time>0.2)) Then  //Aqui se controla la velocidad de disparo
    begin
      time:=0.0;
      Bullet:=TGLSphere(Bullets.AddNewChild(TGLSphere));
      With GetOrCreateCollision(Bullet) Do
        begin
          BoundingMode:=cbmSphere;
          Manager:=CollisionManager1;
        end;
      Bullet.Name:='Bullet'+IntToStr(BCount);
     // Bullet.Name:='Bullet';
      A := Camera.AbsoluteDirection;
      ScaleVector(A, 100);
      B := VectorSubtract(Camera.AbsolutePosition, weap.AbsolutePosition);
      Bullet.AbsolutePosition := weap.AbsolutePosition;
      Bullet.AbsoluteDirection := VectorAdd(A, B);
      Bullet.Move(7);
      Inc(BCount);






    end;
       //Si la distancia entre el MOB y el jugador es menos a 60, que el MOB cambie a posicion de ataque
        if (GLDummyCube1.DistanceTo(GLDummyCube3)<60.0) then
    begin
    //GLDummyCube1.Direction:=GLDummyCube3.Direction;

     if GLActor1.CurrentAnimation<>'attack' then
     begin
     GLActor1.SwitchToAnimation('attack');
     GLActor2.Animations.Assign(GLActor1.Animations);
     GLActor2.Synchronize(GLActor1);
     end;

    end;
       //Si la distancia entre el MOB y el jugador es mayor a 60, que el MOB cambie a posicion inapercibida
    if (GLDummyCube1.DistanceTo(GLDummyCube3)>80.0) then
    begin
    //GLDummyCube1.Direction:=GLDummyCube3.Direction;

     if GLActor1.CurrentAnimation<>'stand' then
     begin
     GLActor1.SwitchToAnimation('stand');
     GLActor2.Animations.Assign(GLActor1.Animations);
     GLActor2.Synchronize(GLActor1);
     end;
    end;



end;

//Este procedimiento realiza la funcion de evaluar la cantidad de cuadros por segundo en tiempo real
procedure TForm1.FPSTimerTimer(Sender: TObject);
begin
  //Se utiliza para que cada que haya una actualizacion el texto no se repita (se actualiza en el mismo lugar)
     //Genera el texto que se muestra al tope de la ventana
   Caption := GLSceneViewer1.FramesPerSecondText;
    //Visualizar la distancia en unidades del dummycube 1 (jugador) al dummycube 2 (actor 2)
    Form1.Caption:= Form1.Caption+ ' Distancia del jugador al MOB '+ floattostr(GLDummyCube1.DistanceTo(GLDummyCube3));
   //Reinicia los contadores de FPS text
   GLSceneViewer1.ResetPerformanceMonitor;

end;


// Este procedimiento se encarga de detectar cuando una bala toca (colisiona) a un objeto
procedure TForm1.CollisionManager1Collision(Sender: TObject; object1,
  object2: TGLBaseSceneObject);
var
  Obj,Bullet:TGLBaseSceneObject;
  Point,Normal:TVector;
begin

//Si una bala colisiona con el actor 1...
if ((object1.Name='Bullet0' ) and (object2.Name='GLActor1')) then
begin
//Inicializar animacion de muerte
  GLActor1.AnimationMode:=aamPlayOnce;
  GLActor1.SwitchToAnimation('death');
  GLActor2.Visible:=false;
  GLActor2.Animations.Assign(GLActor1.Animations);
  GLActor2.Synchronize(GLActor1);
end;

//o si el actor 1 colisiona con una bala..
if ((object2.Name='Bullet0') and (object1.Name='GLActor1')) then
begin
  //Inicializar animacion de muerte
  GLActor1.AnimationMode:=aamPlayOnce;
  GLActor1.SwitchToAnimation('death2');
  GLActor2.Visible:=false;
  GLActor2.Animations.Assign(GLActor1.Animations);
  GLActor2.Synchronize(GLActor1);
end;



  If pos('Bullet',object1.Name)=1 Then
    begin
      Obj:=object2;
      Bullet:=object1;
    end
  Else
    If pos('Bullet',object2.Name)=1 Then
      begin
        Obj:=object1;
        Bullet:=object2;
      end
    Else
      Exit;
  If List.IndexOf(obj.Name)<>-1 Then
    Exit;
    //Coloca una X roja de tama�o 1x1 en el punto donde la bala impacto al actor
  Obj.RayCastIntersect(Bullet.Position.AsVector,Bullet.Direction.AsVector,@Point,@Normal);
  With TGLPlane(World.AddNewChild(TGLPlane)) Do
    begin
      Point[1] := Point[1] + 1;
      AbsolutePosition:=Point;
      AbsoluteDirection:=Normal;
      Width:=1;
      Height:=1;
      Material.MaterialLibrary:=GLMaterialLibrary1;
      Material.LibMaterialName:='mark';
    end;
  List.Add(Bullet.Name);


//Muestra un mensaje de los objetos que colisionaron
ShowMessage('Colision entre '+object1.Name+' y '+object2.Name);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  GLCadencer1.Enabled:=False;
  List.Free;
end;

end.
